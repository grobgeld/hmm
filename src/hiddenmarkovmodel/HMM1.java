/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
//package hiddenmarkovmodel;
import java.io.*;
import java.util.*;

/**
 *
 * @author alexhermansson
 */
public class HMM1 {
    
    public static void main(String[] args) throws Exception {

        FileReader file = new FileReader("/Users/alexhermansson/NetBeansProjects/HiddenMarkovModel/src/hiddenmarkovmodel/hmm2_01.in");
        //BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
        BufferedReader in = new BufferedReader(file);
        
        String line;
        
        List<String> list = new ArrayList<String>();
        while((line = in.readLine()) != null){
            list.add(line);
        }
        
        String[] stringArr = list.toArray(new String[0]);
        // stringArr[0] is the first line of the input
        // stringArr[1] is the second, etc.
        
        double[] arrayA = Matrix.lineToArray(stringArr[0].split(" "));
        double[] arrayB = Matrix.lineToArray(stringArr[1].split(" "));
        double[] arrayPi = Matrix.lineToArray(stringArr[2].split(" "));
        int[] O = Matrix.lineToIntArray(stringArr[3].split(" "));
        
        double[][] A, B, Pi;
        
        int T = O[0]; // Number of time steps
        int N = (int)arrayB[0]; // Number of states
        int M = (int)arrayB[1]; // Number of possible observations
        
        O = Arrays.copyOfRange(O, 1, T+1);
        A = Matrix.createMatrix(arrayA);
        B = Matrix.createMatrix(arrayB);
        Pi = Matrix.createMatrix(arrayPi);
        
        // Initialize alpha
        double[][] alpha = new double[N][T];
        for (int i = 0; i < N; i++) {
            alpha[i][0] = Pi[0][i]*B[i][O[0]];
            
        }
        
        // Calculate alpha iteratively for all t = 1, 2, .. , T
        for (int t = 1; t < T; t++) {
            for (int i = 0; i < N; i++) {
                for (int j = 0; j < N; j++) {
                    alpha[i][t] = alpha[i][t] + alpha[j][t-1]*A[j][i];
                    
                }
                alpha[i][t] = alpha[i][t]*B[i][O[t]];
            }
            
        }  
        
        // Calculate P(Ot:T|A,B,Pi) = sum(alpha(t=T))
        double res = 0;
        for (int i = 0; i < N; i++) {
            res += alpha[i][T-1];
            
        }
        System.out.println(res);
        
    }
    
}
