//package hiddenmarkovmodel;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author alexhermansson
 */
public class ReEstimate {
    
    public static Object[] lambda (double[][][] di_gamma, double[][] gamma, int[] O, int N, int M, int T) {
        
        //Re-estimate A
        double[][] A = new double[N][N];

        for (int i = 0; i < N; i++){
            for (int j=0; j < N; j++){
                double numerator = 0;
                double denominator = 0;

                for (int t=0; t < T-1; t++){
                    numerator += di_gamma[i][j][t];
                    denominator += gamma[i][t];
                }

                A[i][j] = numerator/denominator;
            }
        }

        //re-estimate B
        double[][] B = new double[N][M];
        
        for (int j= 0; j < N; j++){
            for (int k=0; k < M; k++){

                double numerator = 0;
                double denominator = 0;

                for (int t=0; t < T-1; t++) {
                    if (O[t] == k){
                        numerator += gamma[j][t];
                    }
                    denominator += gamma[j][t];
                }

                B[j][k] = numerator/denominator;
            }
        }
        
        // Re-estimate pi
        double[][] Pi = new double[1][N];
        
        for (int i=0; i < N; i++){
            Pi[0][i] = gamma[i][0];
        }
        
        return new Object[] {A, B, Pi};
    }
    
            
            
}
