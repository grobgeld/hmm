/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
//package hiddenmarkovmodel;
import java.io.*;
import java.util.*;

/**
 *
 * @author alexhermansson
 */
public class HMM2 {

    public static void main(String[] args) throws Exception {

        //FileReader file = new FileReader("/Users/alexhermansson/NetBeansProjects/HiddenMarkovModel/src/hiddenmarkovmodel/hmm3_01.in");
        //BufferedReader in = new BufferedReader(file);
        BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
        
        String line;
        
        List<String> list = new ArrayList<String>();
        while ((line = in.readLine()) != null ) {
            list.add(line);
        }
        
        String[] stringArr = list.toArray(new String[0]);
        // stringArr[0] is the first line of the input
        // stringArr[1] is the second, etc.
        
        double[] arrayA = Matrix.lineToArray(stringArr[0].split(" "));
        double[] arrayB = Matrix.lineToArray(stringArr[1].split(" "));
        double[] arrayPi = Matrix.lineToArray(stringArr[2].split(" "));
        double[] doubleArrayO = Matrix.lineToArray(stringArr[3].split(" "));
        int[] O = new int[doubleArrayO.length];
        
        // doubleArrayO has values of type double, cast to int:
        for (int i = 0; i < doubleArrayO.length; i++) {
            O[i] = (int)doubleArrayO[i];
        }
        
        double[][] A, B, Pi;
        int T = O[0]; // Number of time steps
        int N = (int)arrayB[0]; // Number of states
        int M = (int)arrayB[1]; // Number of possible observations
        
        O = Arrays.copyOfRange(O, 1, T+1);
        A = Matrix.createMatrix(arrayA);
        B = Matrix.createMatrix(arrayB);
        Pi = Matrix.createMatrix(arrayPi);

        double[][] delta = new double[N][T];
        int[][] delta_index = new int[N][T];

        // Initialize delta = pi*B(O(0))
        for (int i=0; i < N; i++){
            delta[i][0] = Pi[0][i] * B[i][O[0]];
        }

        // Calculate delta for rest of t
        for (int t=1; t < T; t++){
            for (int i=0; i < N; i++){

                double temp = 0;

                for (int j=0; j < N; j++){
                    double value = A[j][i]*delta[j][t-1]*B[i][O[t]];
                    if (value > temp) {
                        delta[i][t] = value;
                        delta_index[i][t] = j;
                        temp = value;
                    }
                }
            }
        }

        double temp = 0;
        // Initialize X, calculate X[T]
        int[] X  = new int[T];
        for (int j = 0; j < N; j++){
            double value = delta[j][T-1];
            if (value > temp) {
                X[T-1] = j;
                temp = value;
                }
        }
        
        // Go from t = T-1 to t = 0 and calculate X[t] 
        // backwards
        for (int t = T-2; t > -1; t--){
            X[t] = delta_index[X[t+1]][t+1];
        }
        
    String res = "";
    for (int i = 0; i < X.length; i++) {
        res += X[i] + " ";
    }
    res = res.substring(0, res.length() - 1);
    System.out.println(res);

    }

}
