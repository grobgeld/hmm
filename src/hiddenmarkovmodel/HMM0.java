//package hiddenmarkovmodel;
import java.io.*;
import java.util.*;

/**
 *
 * @author alexhermansson
 */
public class HMM0 {

    /**
     * @param args the command line arguments
     */
  
    
    public static void main(String[] args)throws Exception {
        // TODO code application logic here
        //FileReader file = new FileReader("/Users/alexhermansson/NetBeansProjects/HiddenMarkovModel/src/hiddenmarkovmodel/sample_00.in");
        //BufferedReader in = new BufferedReader(file);
        BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
        
        String line;
        
        List<String> list = new ArrayList<String>();
        while((line = in.readLine()) != null){
            list.add(line);
        }
        
        String[] stringArr = list.toArray(new String[0]);
        // stringArr[0] is the first line of the input
        // stringArr[1] is the second, etc.
        
        double[] arrayA = Matrix.lineToArray(stringArr[0].split(" "));
        double[] arrayB = Matrix.lineToArray(stringArr[1].split(" "));
        double[] arrayPi = Matrix.lineToArray(stringArr[2].split(" "));
        
        double[][] A = Matrix.createMatrix(arrayA);
        double[][] B = Matrix.createMatrix(arrayB);
        double[][] Pi = Matrix.createMatrix(arrayPi);
        
        double[][] PiA = Matrix.multiply(Pi, A); // 1xN * NxN = 1xN
        double[][] PiAB = Matrix.multiply(PiA, B); // 1xN * NxM = 1xM
        
        // Printing the result
        Matrix.printMatrixAsLine(PiAB);
        
    }   
        
}

    
