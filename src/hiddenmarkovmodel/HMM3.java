/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
//package hiddenmarkovmodel;
import java.io.*;
import java.util.*;
import java.math.*;

/**
 *
 * @author alexhermansson
 */
public class HMM3 {

    public static void main(String[] args) throws Exception {
        
        //FileReader file = new FileReader("/Users/alexhermansson/NetBeansProjects/HiddenMarkovModel/src/hiddenmarkovmodel/hmm4_01.in");
        //BufferedReader in = new BufferedReader(file);
        BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
        
        String line;
        List<String> list = new ArrayList<String>();
        while ((line = in.readLine()) != null ) {
            list.add(line);
        }
        
        String[] stringArr = list.toArray(new String[0]);
        // stringArr[0] is the first line of the input
        // stringArr[1] is the second, etc.
        
        double[] arrayA = Matrix.lineToArray(stringArr[0].split(" "));
        double[] arrayB = Matrix.lineToArray(stringArr[1].split(" "));
        double[] arrayPi = Matrix.lineToArray(stringArr[2].split(" "));
        int[] O = Matrix.lineToIntArray(stringArr[3].split(" "));
        
        double[][] A, B, Pi;
        int T = O[0]; // Number of time steps
        int N = (int)arrayB[0]; // Number of states
        int M = (int)arrayB[1]; // Number of possible observations
        
        O = Arrays.copyOfRange(O, 1, T+1);
        
        // Step 1.
        // First guess of A, B and Pi. Comes as input.
        A = Matrix.createMatrix(arrayA);
        B = Matrix.createMatrix(arrayB);
        Pi = Matrix.createMatrix(arrayPi);
        
        double oldLogProb = Double.NEGATIVE_INFINITY;
        double logProb;
        int maxIter = 35;
        int iter = 0;
        
        double[][] newA = A;
        double[][] newB = B;
        double[][] newPi = Pi;
        
        while (iter < maxIter) {
        
            // step 2 and 3.
            //System.out.println("oldLogProb: " + oldLogProb);
            
            Object[] abcArray = AlphaBetaPass.alphaBetaC(newA, newB, newPi, O, T);
            double[][] alpha = (double[][])abcArray[0];
            double[][] beta = (double[][])abcArray[1];
            double[] C = (double[])abcArray[2];
            
            //System.out.println("logProb: " + logProb);

            Object[] gammaArray = Gamma.gamma(newA, newB, alpha, beta, O, T);
            double[][] gamma = (double[][])gammaArray[0];
            double[][][] di_gamma = (double[][][])gammaArray[1];
            
            Object[] lambda = ReEstimate.lambda(di_gamma, gamma, O, N, M, T);
            newA = (double[][])lambda[0];
            newB = (double[][])lambda[1];
            newPi = (double[][])lambda[2];
            
            logProb = 0;
            for (int i = 0; i < T; i++) {
                logProb += Math.log(C[i]);   
            }
            logProb = -logProb;
            
            iter++;
            if (iter < maxIter && logProb > oldLogProb) {
                oldLogProb = logProb;
            }
            else {
                Matrix.printMatrixAsLine(newA);
                Matrix.printMatrixAsLine(newB);
                break;
                
            }
            //System.out.println(iter);
            

        }
        //Matrix.printMatrixAsLine(newA);
        //Matrix.printMatrixAsLine(newB);
        
    }

    
}


